<?php   

    $conexion = new PDO('mysql:host=localhost;dbname=prueba_datos','root','', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
    
    
    function logueo($var1){
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM usuarios WHERE id = :id');

        $consulta->execute(
            array(':id'=> $var1)
        );
        $resultado = $consulta->fetch();
        

        return $resultado;
    }

    function guardaInformacion($id,$url){
        global $conexion;

        $guardar = $conexion->prepare('INSERT INTO imagenes ( id, id_user, url)
        VALUES (null, :id_user, :url)'
        );

        $guardar->execute(array(
            ':id_user'=> $id,
            ':url'=> $url
        ));
        
    }

    function agregarLocalRestaUno($id_user){
        
        global $conexion;

        $consulta = $conexion->prepare('UPDATE usuarios SET disponibles = disponibles - 1  WHERE id = :id_user');

        $consulta->execute(
            array(
                    ':id_user'=>$id_user
                )
        );       
    }

    function agregarUsuario($nombre,$cedula,$tipo,$disponibles){
         
         global $conexion;

        $guardar = $conexion->prepare('INSERT INTO usuarios ( id, nombre, cedula, tipo, disponibles)
        VALUES (null, :nombre, :cedula, :tipo, :disponibles)'
        );

        $guardar->execute(array(
            ':nombre'=> $nombre,
            ':cedula'=> $cedula,
            ':tipo'=> $tipo,
            ':disponibles'=> $disponibles
        ));
    }

    function usuarios(){
        
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM usuarios');

        $consulta->execute();

        $resultado = $consulta->fetchAll();
        
        return $resultado;

    }

    function anadirDisponibles($nombre,$numero_disponibles){

        global $conexion;

        $consulta = $conexion->prepare('UPDATE usuarios SET disponibles = disponibles + :numero_disponibles  WHERE nombre = :nombre');

        $consulta->execute(
            array(
                    ':nombre'=>$nombre,
                    ':numero_disponibles'=>$numero_disponibles
                )
        );  

    }

    function suspenderUsuarios($nombre,$nombre_suspende){

        global $conexion;

        $consulta = $conexion->prepare('UPDATE usuarios SET supencion = 1 , supendidopor = :nombre_suspende   WHERE nombre = :nombre');

        $consulta->execute(
            array(
                    ':nombre'=>$nombre,
                    ':nombre_suspende'=>$nombre_suspende
                )
        );

    }
    function comprobarSupencionUsuario($id_user){
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM usuarios  WHERE id = :id_user');

        $consulta->execute(
            array(
                    ':id_user'=>$id_user
                )
        );
        
        $resultado = $consulta->fetch();
        
        return $resultado;
    }
    function quitarSuspencionUsuarios($nombre,$nombre_suspende){

        global $conexion;

        $consulta = $conexion->prepare('UPDATE usuarios SET supencion = 0 , supendidopor = :nombre_suspende   WHERE nombre = :nombre');

        $consulta->execute(
            array(
                    ':nombre'=>$nombre,
                    ':nombre_suspende'=>$nombre_suspende
                )
        );

    }
?>