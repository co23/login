<?php

    function local($var1){
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM inicio WHERE id_local = :id_local');

        $consulta->execute(
            array(':id_local'=> $var1)
        );
        $resultado = $consulta->fetch();
        

        return $resultado;
    }

    function localIdusuario($id_local,$id_usuario){

        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM inicio WHERE id_local = :id_local AND id_user = :id_user');

        $consulta->execute(
            array(
                ':id_local'=>$id_local,
                ':id_user'=>$id_usuario
                )
        );
        $resultado = $consulta->fetch();
        

        return $resultado;

    }

    function categoriaLocal($var1){
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM inicio WHERE id_categoria = :id_categoria');

        $consulta->execute(
            array(':id_categoria'=> $var1)
        );
        $resultado = $consulta->fetchAll();
        

        return $resultado;

    }

    function localNombre($var1){
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM inicio WHERE nombre = :nombre');

        $consulta->execute(
            array(':nombre'=> $var1)
        );
        $resultado = $consulta->fetchAll();
        

        return $resultado;
    }

    function ComprobarLocal($var1,$var2){
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM inicio WHERE id = :id AND nombre = :nombre ');

        $consulta->execute(
            array(
                ':id'=> $var1,
                ':nombre'=>$var2
                )
        );
        
        $resultado = $consulta->fetch();
        
        return $resultado;
    }

    function usarioLocales($var1){
        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM inicio WHERE id_user = :id_user');

        $consulta->execute(
            array(
                ':id_user'=> $var1
                )
        );

        $resultado = $consulta->fetchAll();

        return $resultado;
        
    }

    function actulizarInfoLocal($id, $nombre, $titulo,$url){
        
        global $conexion;

         $consulta = $conexion->prepare('UPDATE inicio SET nombre = :nombre, titulo = :titulo, url = :url  WHERE id_local = :id_local');

        $consulta->execute(
            array(
                    ':nombre'=> $nombre,
                    ':titulo'=>$titulo,
                    ':id_local'=>$id,
                    ':url'=>$url
                )
        );

    }

    function agregarLocal($id_user,$nombre,$titulo,$categoria){
        global $conexion;

        $consulta = $conexion->prepare('INSERT INTO inicio (id, nombre, titulo, id_user, id_local, id_categoria, fechac) 
        VALUES(null, :nombre, :titulo, :id_user, :id_local, :id_categoria, :fecha)');

        $consulta2 = $conexion->prepare('SELECT * FROM inicio');    

        $consulta2->execute();

        $numerolocal = $consulta2->fetchAll();

        $numerolocal = count($numerolocal)+1;

        $fecha = date("d-m-Y", strtotime('+1 years'));

        $consulta->execute(array(
            ':nombre'=> $nombre,
            ':titulo'=> $titulo,
            ':id_user'=>$id_user,
            ':id_local'=>$numerolocal,
            ':id_categoria'=>$categoria,
            ':fecha'=>$fecha
        ));

        return $numerolocal;
    }

    function todosLosLocales(){

        global $conexion;

        $consulta = $conexion->prepare('SELECT * FROM inicio WHERE eliminado = :var ');

         $consulta->execute(array(
            ':var'=> 0
        ));
        
        $resultado = $consulta->fetchAll();
        
        return $resultado;
    }

    function desavilitarLocal($id_local,$id_user){
        
        global $conexion;

        $consulta = $conexion->prepare('UPDATE inicio SET mantenimiento = 1  WHERE id_local = :id_local AND id_user = :id_user');

        $consulta->execute(
            array(
                    ':id_local'=>$id_local,
                    ':id_user'=>$id_user
                )
        );

    }

    function habilitarLocal($id_local,$id_user){
        
        global $conexion;

        $consulta = $conexion->prepare('UPDATE inicio SET mantenimiento = 0  WHERE id_local = :id_local AND id_user = :id_user');

        $consulta->execute(
            array(
                    ':id_local'=>$id_local,
                    ':id_user'=>$id_user
                )
        );

    }

    function eliminarLocal($id_local,$id_user){
        global $conexion;

        $consulta = $conexion->prepare('UPDATE inicio SET eliminado = 1  WHERE id_local = :id_local AND id_user = :id_user');

        $consulta->execute(
            array(
                    ':id_local'=>$id_local,
                    ':id_user'=>$id_user
                )
        );

    }

    function agregarUrlImagenLocalNuevo($id_local,$id_user,$url){
        global $conexion;

        $consulta = $conexion->prepare('UPDATE inicio SET url = :url  WHERE id_local = :id_local AND id_user = :id_user');

        $consulta->execute(
            array(
                    ':id_local'=>$id_local,
                    ':id_user'=>$id_user,
                    ':url'=>$url
                )
        );
    }

    function suspenderLocal($id_local,$nombre){
        global $conexion;

        $fechasuspension = date("d-m-Y");

        $consulta = $conexion->prepare('UPDATE inicio SET suspendido =  1, fechac = :fecha, suspendidopor = :nombre  WHERE id_local = :id_local');

        $consulta->execute(
            array(
                    ':id_local'=>$id_local,
                    ':fecha'=>$fechasuspension,
                    ':nombre'=>$nombre
                )
        );

    }

    function quitarSuspenderLocal($id_local,$nombre){
        global $conexion;

        $fechasuspension = date("d-m-Y");

        $consulta = $conexion->prepare('UPDATE inicio SET suspendido =  0, fechac = :fecha, suspendidopor = :nombre  WHERE id_local = :id_local');

        $consulta->execute(
            array(
                    ':id_local'=>$id_local,
                    ':fecha'=>$fechasuspension,
                    ':nombre'=>$nombre
                )
        );

    }
?>