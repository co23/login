<?php 

    function categorias(){
        global $conexion;

        $consulta = $conexion->prepare("SELECT * FROM categorias");

        $consulta->execute();

        $resultado = $consulta->fetchAll();
        
        return $resultado;
    }

    function categoriaNombre($var1){
        global $conexion;

        $consulta = $conexion->prepare("SELECT * FROM categorias WHERE nombre = :nombre");

        $consulta->execute(array(':nombre'=>$var1));

        $resultado = $consulta->fetch();
        
        return $resultado;

    }

?>    