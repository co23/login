<?php
    function crearCarpeta($ruta){
        
        if (!file_exists($ruta)) {
            mkdir($ruta, 0777, true);
        }

        return "carpeta creada";
    }
    

    function comprobarConexion(){
        if($_SESSION){
            
        }else{
            header('Location: login');
        }
    }

    function subirImagenes($tmp_name, $id_user, $name,$id_local){

        $nombrearchivo = $tmp_name;

        $ruta = 'source/'.$id_user.'/'.$id_local.'/imagenes/'.rand().$name;

        $rutaCarpeta = 'source/'.$id_user.'/'.$id_local.'/imagenes/';

        crearCarpeta($rutaCarpeta); 

        move_uploaded_file($nombrearchivo,$ruta);
        
        return $ruta;
    }

    function alertaFechas($fecha_final){
        $datetime1 = new DateTime($fecha_final);
        $datetime2 = new DateTime(date('d-m-Y'));
        $interval = $datetime1->diff($datetime2);
        $dias_faltan = $interval->format('%a');
        return $dias_faltan;
    }

?>